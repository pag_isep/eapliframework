/*
 * Copyright (c) 2013-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package eapli.framework.money.domain.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

/**
 * @author Paulo Gandra de Sousa 2023.03.31
 */
class CashFlowTest {

    @Test
    void ensureMoneyIsMandatory() {
        // ARRANGE
        final var now = LocalDate.now();

        // ACT - ASSERT
        assertThrows(IllegalArgumentException.class, () -> new CashFlow(now, null));
    }

    @Test
    void ensureDateIsMandatory() {
        // ARRANGE
        final var two = Money.dollars(2);

        // ACT - ASSERT
        assertThrows(IllegalArgumentException.class, () -> new CashFlow(null, two));
    }

    @Test
    void ensureAddOnTheSameDay() {
        // ARRANGE
        final var today = LocalDate.now();
        final var a = new CashFlow(today, Money.dollars(1));
        final var b = new CashFlow(today, Money.dollars(2));
        final var expected = new CashFlow(today, Money.dollars(3.00));

        // ACT
        final var result = a.add(b);

        // ASSERT
        assertEquals(expected, result);
    }

    @Test
    void ensureCannotAddOnDifferentDays() {
        // ARRANGE
        final var today = LocalDate.now();
        final var a = new CashFlow(today, Money.dollars(1));
        final var b = new CashFlow(today.minusDays(1), Money.dollars(2));

        // ACT - ASSERT
        assertThrows(IllegalArgumentException.class, () -> a.add(b));
    }

    @Test
    void ensureSubtractOnTheSameDay() {
        // ARRANGE
        final var today = LocalDate.now();
        final var a = new CashFlow(today, Money.dollars(1));
        final var b = new CashFlow(today, Money.dollars(2));
        final var expected = new CashFlow(today, Money.dollars(-1.00));

        // ACT
        final var result = a.subtract(b);

        // ASSERT
        assertEquals(expected, result);
    }

    @Test
    void ensureCannotSubtractOnDifferentDays() {
        // ARRANGE
        final var today = LocalDate.now();
        final var a = new CashFlow(today, Money.dollars(1));
        final var b = new CashFlow(today.minusDays(1), Money.dollars(2));

        // ACT - ASSERT
        assertThrows(IllegalArgumentException.class, () -> a.subtract(b));
    }

    @Test
    void whenTwoCashFlowsHaveSameAmountAndSameDateThenCompareIs0() {
        // ARRANGE
        final var today = LocalDate.now();
        final var amt = Money.dollars(1);
        final var a = new CashFlow(today, amt);
        final var b = new CashFlow(today, amt);

        // ACT - ASSERT
        assertEquals(0, a.compareTo(b));
    }

    @Test
    void whenACashFlowHasPreviousDateThanAnotherThenCompareIsMinus1() {
        // ARRANGE
        final var today = LocalDate.now();
        final var amt = Money.dollars(1);
        final var a = new CashFlow(today.minusDays(1), amt);
        final var b = new CashFlow(today, amt);

        // ACT - ASSERT
        assertEquals(-1, a.compareTo(b));
    }

    @Test
    void whenACashFlowHasPosteriorDateThanAnotherThenCompareIs1() {
        // ARRANGE
        final var today = LocalDate.now();
        final var amt = Money.dollars(1);
        final var a = new CashFlow(today.plusDays(1), amt);
        final var b = new CashFlow(today, amt);

        // ACT - ASSERT
        assertEquals(1, a.compareTo(b));
    }

    @Test
    void whenTwoCashFlowsHaveTheSameDateButOneHasASmallerAmountThenCompareIsMinus1() {
        // ARRANGE
        final var today = LocalDate.now();
        final var amt1 = Money.dollars(1);
        final var amt2 = Money.dollars(2);
        final var a = new CashFlow(today, amt1);
        final var b = new CashFlow(today, amt2);

        // ACT - ASSERT
        assertEquals(-1, a.compareTo(b));
    }

    @Test
    void whenTwoCashFlowsHaveTheSameDateButOneHasABiggerAmountThenCompareIs1() {
        // ARRANGE
        final var today = LocalDate.now();
        final var amt1 = Money.dollars(10);
        final var amt2 = Money.dollars(2);
        final var a = new CashFlow(today, amt1);
        final var b = new CashFlow(today, amt2);

        // ACT - ASSERT
        assertEquals(1, a.compareTo(b));
    }
}
